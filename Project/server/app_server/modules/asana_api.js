"use strict";


var asana = require('asana');
var asanaApi = require('asana-api');
//var request = require('request');


module.exports = function () {

    this.ModuleName = "Asana_Api";

    var restifyHelper = global.RestifyHelper;
    var ASANA_API = null;
    var config = Nix.Config;
    var accessToken = config.personal_access_token;
    var client = asana.Client.create().useAccessToken(accessToken);

    var users_client = asanaApi.createClient({
        token: accessToken
    });


    var userID = null;
    var workspaceID = null;
    var allWorkspaces = {};
    var allUsers = {};

    this.Init = function () {
        ASANA_API = Nix.Modules.Asana_Api;
    };
   
    var allProjects;
    var allTasks;
    restifyHelper.AddGetService('/get/projects', false, function (req, res, next) {
        function projectsData(projectsData) {
            res.send(projectsData);
        }

        function cycleProducts(projects, counter, retValue) {
            if (projects.length == counter) {
                projectsData(retValue);
                return;
            }

            client.projects.findById(projects[counter].id).then(function (data) {
                retValue.push(data);
                cycleProducts(projects, counter + 1, retValue);
            }, function (err) {
                cycleProducts(projects, counter + 1, retValue);
            });
        }

        client.users.me().then(function (user) {
            userID = user.id;
            workspaceID = user.workspaces[0].id;
            return client.projects.findAll({
                workspace: workspaceID
            }).then(function (projects, err) {
                allProjects = projects.data;
                cycleProducts(allProjects, 0, []);
            });

        });
    });


    //var projectID = 244434463943839;
    restifyHelper.AddGetService('/get/project/:projectid/tasks', false, function (req, res, next) {
        function tasksData(tasksData) {
            res.send(tasksData);
        }

        function cycleTasks(tasks, counter, retValue) {
            if (tasks.length == counter) {
                tasksData(retValue);
                return;
            }
            client.tasks.findById(tasks[counter].id).then(function (data) {
                retValue.push(data);
                cycleTasks(tasks, counter + 1, retValue);
            }, function (err) {
                cycleTasks(tasks, counter + 1, retValue);
            });
        }
        client.users.me().then(function (user) {
            userID = user.id;
            workspaceID = user.workspaces[0].id;
            var projectID = req.params.projectid;
            return client.tasks.findAll({
                project: projectID
            }).then(function (tasks, err) {
                allTasks = tasks.data;
                cycleTasks(allTasks, 0, []);
                //if (err) {
                //    restifyHelper.RespondError(res, 409, 'DB Error.');
                //}
                //else {
                //    restifyHelper.RespondSuccess(res, tasks.data)
                //}
            });

        });
    });

    //
    //function getAllUsers() {
    //    users_client.users.list(function (err, users) {
    //        allUsers = users;
    //        console.log('Users');
    //        console.log(allUsers);
    //
    //    })
    //}
    //
    //getAllUsers();
    //getAllWorkspaces();
    //getAllProjects();


};
