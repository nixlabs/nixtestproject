'use strict';

app.factory("Project", ['$http', '$rootScope', function ($http, $rootScope) {
    var urlBase = $rootScope.serverUrl;

    return {
        getAllProjects: function () {
            return $http
            ({
                method: 'GET',
                url: urlBase + '/get/projects',
                withCredentials: true
            });
        },
        getProjectTasks: function (id) {
            return $http
            ({
                method: 'GET',
                url: urlBase + '/get/project/' + id + '/tasks',
                withCredentials: true
            });
        }
    }
}]);
