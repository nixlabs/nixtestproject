﻿var Nix = Nix || {};
Nix.DAL = Nix.DAL || {};


Nix.DAL.GetMetaobjectByObjectType = function (ObjectType){
    var keys = Object.keys(Nix.DAL);
    for (var i = 0; i < keys.length; i++) {
        if (Nix.DAL[keys[i]].NixObjectType == ObjectType) {
            return Nix.DAL[keys[i]];
        }
    };

    return null;
};