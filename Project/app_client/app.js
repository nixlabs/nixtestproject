var externalModules = [
    'ui.router',
    'ngToast',
    'mgcrea.ngStrap',
    'NixDataModule',
    'ui.bootstrap',
    'angular-confirm',
    'isteven-multi-select',
    'ngStorage',
    'ngAria',
    'ngMaterial',
    'md.data.table',
    'ngTouch',
    'angularMoment',
    'ngMaterialDatePicker',
    'mdPickers',
    'ngFileUpload',
    'ngFileSaver',
    'cgBusy',
    'highcharts-ng'
];
var app = angular.module("MainApp", externalModules);

app.config(function ($datepickerProvider) {
    angular.extend($datepickerProvider.defaults, {
        dateFormat: 'dd/MM/yyyy',
        startWeek: 1
    });
});

//app.config(['$tooltipProvider', function ($tooltipProvider) {
//        $tooltipProvider.setTriggers({
//            'mouseenter': 'mouseleave',
//            'click': 'click',
//            'focus': 'blur',
//            'never': 'mouseleave' // <- This ensures the tooltip will go away on mouseleave
//        });
//    }]);


Array.prototype.NixFilter = function (field, value) {
    for (var i = 0 in this) {
        if (this[i][field] == value) {
            return this[i];
        }
    }
    return null;
};

app.run(['$rootScope', 'ngToast', '$localStorage', '$q', '$location', '$mdToast',
    function ($rootScope, ngToast, $localStorage, $q, $location, $mdToast) {
        $rootScope.PageTitle = "Nix ERP";
        $rootScope.currentState = null;
        $rootScope.activeStates = [];
        $rootScope.cacheInit = false;

        $rootScope.User = null;

        $rootScope.serverPath = window.location.hostname + ":3000";
        $rootScope.serverUrl = "http://" + window.location.hostname + ":3000";

        $rootScope.BackendOnline = false;

        try {
            $rootScope.BackendOnline = angular.isDefined(io);
        }
        catch (ex) {
            console.error('NixERP Backend is offline!')
        }

        $rootScope.IsModelEnabled = function (modelName) {
            if ($rootScope.User != null && $rootScope.User.Permissions.AllowedModels[modelName]) {
                return true;
            }

            return false;
        };

        $rootScope.isAuthenticated = function () {
            if ($localStorage.NixERPData && $localStorage.NixERPData.User && $localStorage.NixERPData.User != $rootScope.User) {
                $rootScope.User = $localStorage.NixERPData.User;
            }

            return $rootScope.User != null;
        };

        //ngToast.showSuccess = function (msg) {
        //    this.create({
        //        content: msg,
        //        className: 'success',
        //        dismissButton: true,
        //        animation: 'fade',
        //        verticalPosition: 'bottom',
        //        horizontalPosition: 'left'
        //    });
        //};
        //
        //ngToast.showError = function (msg) {
        //    this.create({
        //        content: msg,
        //        className: 'danger',
        //        dismissButton: true,
        //        animation: 'fade',
        //        verticalPosition: 'bottom',
        //        horizontalPosition: 'left'
        //    });
        //};

        $rootScope.showActionToast = function (Text) {
            var toast = $mdToast.simple()
                .textContent(Text)
                .position("top right");
            $mdToast.show(toast);
        };
        $rootScope.Refresh = function () {
            location.reload();
        };
    }]);



if(Nix && Nix.DAL){
    var nixObjectTypes = Object.keys(Nix.DAL);
    Nix.ObjectTypes = {};
    for (var i = 0; i < nixObjectTypes.length; i++) {
        var tmpType = Nix.DAL[nixObjectTypes[i]];
        Nix.ObjectTypes[tmpType.NixObjectType] = tmpType;
    }
}
