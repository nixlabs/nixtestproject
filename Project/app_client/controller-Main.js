"use strict"

app.controller('MainCtrl', ['$scope', 'socket', '$rootScope', '$localStorage', '$state', '$mdSidenav', 'NixDataCache', 'RestifyHelper',
    function ($scope, socket, $rootScope, $localStorage, $state, $mdSidenav, NixDataCache, RestifyHelper) {

        $rootScope.Logout = function () {
            $localStorage.NixERPData = null;
            $rootScope.User = null;
            socket.disconnect();
        };

        $rootScope.HasModelPermissions = function (model) {
            if ($rootScope.User == null) {
                return false;
            }
            return $rootScope.User.Permissions.AllowedModels[model] != undefined;
        }

        function BuildMainMenu() {
            if ($rootScope.mainMenu)
                return;

            $rootScope.mainMenu = [
                {
                    Title: "Projects",
                    Group: "projects",
                    CssClass: "fa fa-money",
                    Children: []
                }
            ];

            var tmpMenu = {};
            for (var i = 0; i < $rootScope.mainMenu.length; i++) {
                var tmpItem = $rootScope.mainMenu[i];
                tmpMenu[tmpItem.Group] = tmpItem;
            }

            var states = $state.get();
            for (var i = 0; i < states.length; i++) {
                var tmpState = states[i];


                if (tmpState.ChildrenStates == undefined) {
                    tmpState.ChildrenStates = [];
                }
                if (tmpState.ParentState == undefined) {
                    tmpState.ParentState = null;
                }
                if (tmpState.ParentGroup == undefined) {
                    tmpState.ParentGroup = null;
                }

                if (tmpState.menuGroup && tmpMenu[tmpState.menuGroup]) {
                    tmpState.ParentGroup = tmpMenu[tmpState.menuGroup];
                    tmpMenu[tmpState.menuGroup].Children.push(tmpState);
                }

                if (tmpState.parent) {
                    var tmpParentState = $state.get(tmpState.parent);
                    if (tmpParentState) {
                        if (tmpParentState.ChildrenStates == undefined) {
                            tmpParentState.ChildrenStates = [];
                        }

                        tmpParentState.ChildrenStates.push(tmpState);
                        tmpState.ParentState = tmpParentState;
                    }
                }
            }

            for (var i = 0; i < $rootScope.mainMenu.length; i++) {
                $rootScope.mainMenu[i].Children.sort(function (a, b) {
                    return a.Order > b.Order;
                });
            }
        }

        BuildMainMenu();


        $rootScope.SetCurrentState = function (curState) {
            $rootScope.currentState = curState;
            $rootScope.activeStates = [];

            var tmpState = curState;
            while (tmpState.ParentState != null) {
                $rootScope.activeStates.push(tmpState);
                tmpState = tmpState.ParentState;
            }

            $rootScope.activeStates.push(tmpState);

            if (tmpState.ParentGroup != null) {
                $rootScope.activeStates.push(tmpState.ParentGroup);
            }

            $rootScope.activeStates.reverse();
        }

        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                $rootScope.SetCurrentState(toState);
            });


        $rootScope.IsGroupSelected = function (menuGroup) {
            return $rootScope.activeStates.indexOf(menuGroup) > -1;
        };

        $rootScope.RootMenuItemClicked = function (menuItem) {
            for (var i = 0; i < menuItem.Children.length; i++) {
                var tmpItem = menuItem.Children[i];
                if ($rootScope.HasModelPermissions(tmpItem.permissions)) {
                    $state.go(tmpItem.name);
                    return;
                }
            }

            return;
        };

        $rootScope.RootHasModelPermissions = function (menuItem) {
            for (var i = 0; i < menuItem.Children.length; i++) {
                var tmpItem = menuItem.Children[i];
                if ($rootScope.HasModelPermissions(tmpItem.permissions)) {
                    return true;
                }
            }

            return false;
        };


        $rootScope.Logs = [];
        $rootScope.showLogs = function () {
            var objecttype = $state.current.nixobjecttype;
            var tmpParams = Object.keys($state.params);
            var id = $state.params[tmpParams[tmpParams.length - 1]];
            var promise = RestifyHelper.get_logs(objecttype, id);
            promise.then(function (result) {
                $rootScope.Logs = result;
                console.log(result);
            });
        };

        $rootScope.MenuItemClicked = function (menuItem) {
            $state.go(menuItem.name);
        };


        $rootScope.openSideNavPanel = function () {
            $mdSidenav('left').open();
        };

        $rootScope.closeSideNavPanel = function () {
            $mdSidenav('left').close();
        };

        $scope.mdSidenavIsOpen = function () {
            return $mdSidenav('left').isLockedOpen();
        };

        $rootScope.toggleRight = function () {
            $mdSidenav('right').toggle();
            $scope.showLogs();
        };

        $rootScope.close = function () {
            $mdSidenav('right').close();
        };

        $rootScope.isOpenRight = function () {
            return $mdSidenav('right').isOpen();
        };

        $scope.checkLogsStatus = function () {
            if (Object.keys($state.params).length > 0 && $state.current.nixobjecttype != undefined) {
                return true;
            }
            else {
                return false;
            }
        };

        $rootScope.GetActiveClass = function (item) {
            if ($rootScope.currentState == item) {
                return "subactive active";
            }
            if ($rootScope.activeStates.indexOf(item) > -1) {
                return "subactive";
            }
            return "";
        };
    }]);