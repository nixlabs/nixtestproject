"use strict";

app.controller('ProjectDetailsCtrl', ['$scope', '$state', '$stateParams', 'NixDataProvider', '$rootScope', 'Project',
    function ($scope, $state, $stateParams, NixDataProvider, $rootScope, Project) {
        $rootScope.PageTitle = "Project Tasks";
        $scope.ProjectTasks = [];
        $scope.CurrentID = null;

        var dc_Tasks = NixDataProvider.getDataContext(Nix.DAL.Task);

        $scope.goToDetails = function (row) {
            $state.go('Task_Details', {id: row.id});
        };


        $scope.gridConfig = new Nix.Config.NixFormConfig();
        $scope.gridConfig.RowClicked = $scope.goToDetails;
        $scope.gridConfig.ShowLabels = true;
        $scope.gridConfig.ShowPlaceholders = true;

        $scope.gridConfig.NixFields = [
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Task.Fields.id, "#"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Task.Fields.name, "Name"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Task.Fields.created_at, "Created At"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Task.Fields.due_on, "Due On"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Task.Fields.completed, "Completed")
        ];
        //
        //var dataContextCurrencies = NixDataProvider.getDataContext(Nix.DAL.Currencies);
        //var dataContextProducts = NixDataProvider.getDataContext(Nix.DAL.Products);
        //

        $scope.currentID = $stateParams.projectid;
        var promise1 = Project.getProjectTasks($scope.currentID);
        promise1.then(function (res) {
            $scope.ProjectTasks = NixDataProvider.getDataContext(res.data);
            var promise = NixDataProvider.loadNixObjects(dc_Tasks);
            promise.then(function (data) {
                //$scope.ProjectList = dc_Projects.Data;
                $scope.gridConfig.NixData = $scope.ProjectTasks.NixDataObject;
                console.log(data);
            }, function (reason) {
                console.log("Error: " + reason);
            });
        });


    }]);