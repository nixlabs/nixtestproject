"use strict";

app.controller('ProjectListCtrl', ['$scope', '$state', '$stateParams', 'NixDataProvider', '$rootScope', 'Project',
    function ($scope, $state, $stateParams, NixDataProvider, $rootScope, Project) {
        $rootScope.PageTitle = "Projects";
        $scope.ProjectList = [];

        var dc_Projects = NixDataProvider.getDataContext(Nix.DAL.Project);

        $scope.goToDetails = function (row) {
            $state.go('Project_Details', {projectid: row.id});
        };


        $scope.gridConfig = new Nix.Config.NixFormConfig();
        $scope.gridConfig.RowClicked = $scope.goToDetails;
        $scope.gridConfig.ShowLabels = true;
        $scope.gridConfig.ShowPlaceholders = true;

        $scope.gridConfig.NixFields = [
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Project.Fields.id, "#"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Project.Fields.name, "Name"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Project.Fields.created_at, "Created At"),
            new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.Project.Fields.due_date, "Due Date")
        ];
        //
        //var dataContextCurrencies = NixDataProvider.getDataContext(Nix.DAL.Currencies);
        //var dataContextProducts = NixDataProvider.getDataContext(Nix.DAL.Products);
        //
        var promise1 = Project.getAllProjects();
        promise1.then(function (res) {
            $scope.ProjectList = NixDataProvider.getDataContext(res.data);
            var promise = NixDataProvider.loadNixObjects(dc_Projects);
            promise.then(function (data) {
                //$scope.ProjectList = dc_Projects.Data;
                $scope.gridConfig.NixData = $scope.ProjectList.NixDataObject;
                console.log(data);
            }, function (reason) {
                console.log("Error: " + reason);
            });
        });


    }]);