"use strict";

app.controller('UserLoginCtrl', ['$scope', '$state', '$stateParams', 'NixDataProvider', 'NixDataObject', 'ngToast', 'NixUserHelper', '$rootScope', '$localStorage', 'socket',
    function ($scope, $state, $stateParams, NixDataProvider, NixDataObject, ngToast, NixUserHelper, $rootScope, $localStorage, socket) {
        $rootScope.PageTitle = "Login";
        $scope.InvalidAuthentication = false;

        $scope.User = {
            Username: "",
            Password: ""
        };

        $scope.Error = null;

        $scope.Login = function () {
            var promise = NixUserHelper.Authenticate($scope.User);
            promise.then(function (res) {
                if (res.data) {
                    $localStorage.NixERPData = res.data;
                    socket.connect();
                    $state.go("Project_List");
                }
            }, function (res) {
                $scope.Error = res.data;
                $scope.InvalidAuthentication = true;
            });
        };
    }
]);
