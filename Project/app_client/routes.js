app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {

        var requireAuthentication = function () {
            return {
                load: function ($q) {
                    if ($rootScope.Authenticated) {
                        return deferred.promise;
                    } else {
                        return $q.reject("'/login'");
                    }
                }
            };
        };

        $urlRouterProvider.otherwise("/projects");

        $stateProvider
            //.state('Home', {
            //    url: '/home',
            //    views: {
            //        '@': {
            //            templateUrl: '/Templates/Home/home.html',
            //            controller: 'HomeCtrl'
            //        }
            //    }
            //})
            .state('Users_Login', {
                url: '^/login',
                views: {
                    '@': {
                        templateUrl: '/Templates/Users/user-login.html',
                        controller: 'UserLoginCtrl'
                    }
                }
            })
            .state('Project_List', {
                url: '/projects',
                menuGroup: 'projects',
                menuOrder: 1,
                menuTitle: 'Projects',
                views: {
                    '@': {
                        templateUrl: 'Templates/Project/project-list.html',
                        controller: 'ProjectListCtrl'
                    }
                }
            })
            .state('Project_Details', {
                url: '^/project/:projectid/tasks',
                parent: 'Project_List',
                views: {
                    '@': {
                        templateUrl: 'Templates/Project/project-details.html',
                        controller: 'ProjectDetailsCtrl'
                    }
                }
            })

            .state('Task_List', {
                parent: 'Task_Details',
                url: '/project/:projectid/tasks',
                views: {
                    '@': {
                        templateUrl: 'Templates/Tasks/task-list.html',
                        controller: 'TaskListCtrl'
                    }
                }
            })
            .state('Task_Details', {
                url: '^/project/:projectid/tasks/:id',
                parent: 'Task_List',
                views: {
                    '@': {
                        templateUrl: 'Templates/Tasks/task-details.html',
                        controller: 'TaskDetailsCtrl'
                    }
                }
            });


        $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.NixERPData && $localStorage.NixERPData.Token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.NixERPData.Token;
                    }
                    return config;
                },
                'responseError': function (response) {
                    if (response.status === 401 || response.status === 403) {
                        $localStorage.NixERPData = null;
                    }
                    return $q.reject(response);
                }
            };
        }]);

    }
]);